import pygame
from common_variables import *


# Class for the ball
class Ball(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((16, 16))  # Size of the ball
        self.image = pygame.image.load(
            "resources/images/balls/16x16/ball.png"
        ).convert_alpha()  # Color of the ball
        self.rect = self.image.get_rect()
        self.launched = False  # Flag to track launch state
        self.rect.center = (x, y)  # Position (fixed launch point for now)
        self.vx = 0  # Initial horizontal velocity
        self.vy = 0  # Initial vertical velocity

    def update(self):
        self.vx += 0.1  # Add a slight horizontal drift
        self.vy += 0.5  # Add gravity to vertical velocity
        self.rect.x += self.vx
        self.rect.y += self.vy

        # Check for bouncing off edges
        if self.rect.left <= 0 or self.rect.right >= SCREEN_WIDTH:
            self.vx = -self.vx

        if self.rect.top <= 0:
            self.vy = -self.vy
