import pygame
from common_variables import *


# Launcher class (placeholder for now)
class Launcher(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((20, 20))  # Size of the launcher (visual aid)
        self.image = pygame.image.load(
            "resources/images/aim_block_rendered_end.png"
        ).convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)  # Fixed position
