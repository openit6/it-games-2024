import pygame
from Ball import Ball
from Launcher import Launcher
from common_variables import *
import math

pygame.init()

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

pygame.display.set_caption("Peggle Tutorial")

### BACKGROUND
# Load background image
background_image = pygame.image.load(
    "resources/images/alt_background960x720.jpg"
).convert()


# Resize the image to fit the screen if needed
if (
    background_image.get_width() != SCREEN_WIDTH
    or background_image.get_height() != SCREEN_HEIGHT
):
    background_image = pygame.transform.scale(
        background_image, (SCREEN_WIDTH, SCREEN_HEIGHT)
    )

### ICON
icon_image = pygame.image.load("resources/images/icons/ball.png").convert_alpha()
icon_image.set_colorkey((255, 0, 255))
pygame.display.set_icon(icon_image)

### LAUNCHER
launcher_group = pygame.sprite.Group()
ball_group = pygame.sprite.Group()
launcher = Launcher(SCREEN_WIDTH / 2, screen.get_rect().top + 50)

launcher_group.add(launcher)

### APP
app_running = True
delta_time = 0.0
# Clock object for frame rate control
clock = pygame.time.Clock()

mouse_position = (0, 0)
ball = Ball(0, 0)  # Initial position at (0, 0) (not visible yet)

while app_running:
    # Draw background image on the screen
    launcher_group.draw(background_image)
    screen.blit(background_image, (0, 0))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            app_running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                app_running = False
        elif event.type == pygame.KEYUP:
            pass
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == pygame.BUTTON_LEFT:
                # Launch the ball on left click
                mouse_pos = pygame.mouse.get_pos()
                if not ball.launched:  # Check if not already launched
                    # Set initial launch angle based on mouse click position relative to launcher
                    # (replace with your desired launch angle calculation based on mouse position)
                    ball.rect.center = (
                        launcher.rect.center
                    )  # Move ball to launcher position
                    ball.vx = 50 * math.cos(
                        math.atan2(
                            mouse_pos[1] - launcher.rect.centery,
                            mouse_pos[0] - launcher.rect.centerx,
                        )
                    )
                    ball.vy = 10 * math.sin(
                        math.atan2(
                            mouse_pos[1] - launcher.rect.centery,
                            mouse_pos[0] - launcher.rect.centerx,
                        )
                    )
                    ball_group.add(ball)
                    ball.launched = True  # Set launched flag

    #
    # Draw ball only if launched
    if ball.launched:
        ball_group.draw(background_image)
    else:
        ball_group.clear(screen, bgd=background_image)

    # Update ball
    ball_group.update()

    pygame.display.flip()

    delta_time = 0.001 * clock.tick(144)

pygame.quit()
